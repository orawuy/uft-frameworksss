﻿' Get date time for execute all test

Call FW_StartExecute()
strTag = "Run"

'********************************************************************************
'Initial Start Run
'Create Initial FrameWork	(Define Setup Parameter & Import TestData)
'********************************************************************************
Call FW_CreateInitialFramework()
Call FW_WriteLog("FW_CreateInitialFramework", "OK")
Call FW_WriteLog("FW_CreateInitialFramework", "SUCCESS")
Call FW_WriteLog("StartExecuteTime", startExecuteTime)
If strTag = "" Then
	strTag = "Run"
End If

tag = Split(strTag, ",")
tagArrayCount = UBound(tag)

Call FW_WriteLog("GetRowCount", DataTable.GlobalSheet.GetRowCount)

For dtRow = 1 To DataTable.GlobalSheet.GetRowCount Step 1
	DataTable.GlobalSheet.SetCurrentRow(dtRow)
	tagAction = UCase(DataTable("Tag", dtGlobalsheet))
	testCase = Trim((DataTable("TestCase", dtGlobalsheet)))
	testName = Trim((DataTable("TestName", dtGlobalsheet)))
	
	Call FW_WriteLog("Tag", tagAction)
	Call FW_WriteLog("TestCase", testCase)
	Call FW_WriteLog("TestName", testName)
	
	For index = 0 to tagArrayCount Step 1
		If Instr(1, tagAction, UCase(Trim(tag(index)))) <> 0 or Trim(tag(index)) = "run" Then
			DataTable(RESULT_STATUS,dtGlobalsheet) = "Run"
		
			'Define Parameter for Create Folder TestCase for Get Snapshot per Test Case
			strResultPathTestCase = strResultPathExecution
			
			'Create Folder before Run
			Call CreateFolder(strResultPathTestCase)
			Call FW_WriteLog("Row Execution " & Datatable.GlobalSheet.GetCurrentRow & VbNewLine & "StartTime : " & Now() & VbNewLine & "TestCase : " & testCase & VbNewLine & "TestName : " & testName & VbNewLine & "TestCase Result (CaptureScreenShot) : " & strResultPathTestCase, "")
			
			'Call Function from DataTable Column TestCase
			Select Case testCase
				'TC01	
				'Case "TC01"
				
				'TC02	
				'Case "TC02"
				'TC03	
				'Case "TC03"
				'TC04	
				Case "TC04"
					RunAction "TC04_Xcart [TC04_Xcart]", oneIteration
				'TC05	
				Case "TC05"
					
			End Select
		
			'Call Function Stamp Result from Report to Datatable Column 'Desctiption' 			
			Call FW_CreateResult()

			Exit For
			
		ElseIf index = tagArrayCount Then
			'Create Status not run 
			DataTable(RESULT_STATUS,dtGlobalSheet) =  "No Run"
			Exit For
		Else
			DataTable(RESULT_STATUS, dtGlobalSheet) = tagAction & " <> " & tag(index) & " No Run"
			
		End If
	Next
Next

'Export Result from Datatable
Call FW_StopExecute()
Call FW_WriteLog("FW_StopExecute", "OK")
Call FW_WriteLog("FW_StopExecute", "SUCCESS")
Call FW_WriteLog("StopExecuteTime", stopExecuteTime)

Call FW_ExportResult()
Print "Execution Completed [" & Now & "]"
Call FW_WriteLog("Execution Completed", "[" & Now & "]")
'FW_ExportReportToDoc()
'FW_StopExecute()
'********************************************************************************
