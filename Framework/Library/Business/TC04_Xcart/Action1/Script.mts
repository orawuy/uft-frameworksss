﻿'--------------------- Data ---------------------------------------'
iRowCount = Datatable.getSheet("TC04_Xcart [TC04_Xcart]").getRowCount
Username = Trim((DataTable("Username","TC04_Xcart [TC04_Xcart]")))
Password = Trim((DataTable("Password","TC04_Xcart [TC04_Xcart]")))
OrderNote = Trim((DataTable("OrderNote","TC04_Xcart [TC04_Xcart]")))

'--------------------- Data ---------------------------------------'

Call FW_TransactionStart ("OpenWeb")
Call FW_OpenWebBrowser ("https://demostore.x-cart.com/","CHROME") @@ script infofile_;_ZIP::ssf2.xml_;_
Call FW_TransactionEnd ("OpenWeb")


Call FW_TransactionStart ("Sign in")
Call FW_WebButton ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Sign in / sign up")
Call FW_TransactionEnd ("Sign in")

Call FW_TransactionStart ("Input Username")
Call FW_WebEdit ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","username",Username)
Call FW_TransactionEnd ("Input Username")

Call FW_TransactionStart ("Input Password")
Call FW_WebEdit ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","password",Password)
Call FW_TransactionEnd ("Input Password")

Call FW_TransactionStart ("Submit Login")
Call FW_WebButton ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Sign in") @@ hightlight id_;_Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("{'url params':{'target':'login")_;_script infofile_;_ZIP::ssf3.xml_;_
Call FW_TransactionEnd ("Submit Login")

'FW_TransactionStart "Click Hot deals"
'FW_WebElement "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Hot deals" @@ hightlight id_;_Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("{'url params':{'target':'login")_;_script infofile_;_ZIP::ssf6.xml_;_
'FW_TransactionEnd "Click Hot deals"

Call FW_TransactionStart ("Select Bestsellers")
Call FW_Link ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Bestsellers")
Call FW_TransactionEnd ("Select Bestsellers") @@ script infofile_;_ZIP::ssf20.xml_;_

'FW_TransactionStart "Click Beauty & Health"
'FW_Link "TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Beauty & Health" @@ script infofile_;_ZIP::ssf21.xml_;_
'FW_TransactionEnd "Click Beauty & Health"

Call FW_TransactionStart ("Select Cosmetics")
Call FW_Link ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Cosmetics") @@ script infofile_;_ZIP::ssf22.xml_;_
Call FW_TransactionEnd ("Select Cosmetics")

Call FW_TransactionStart ("Select Maui Moisture")
Call FW_Image ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Maui Moisture")
Call FW_TransactionEnd ("Select Maui Moisture") @@ script infofile_;_ZIP::ssf23.xml_;_

Call FW_TransactionStart ("Add to cart")
Call FW_WebButton ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Add to cart") @@ script infofile_;_ZIP::ssf24.xml_;_
Call FW_TransactionEnd ("Add to cart")

Call FW_TransactionStart ("View cart")
Call FW_Link ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","View cart")
Call FW_TransactionEnd ("View cart")
 @@ script infofile_;_ZIP::ssf25.xml_;_
Call FW_TransactionStart ("Go to checkout")
Call FW_WebButton ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Go to checkout")
Call FW_TransactionEnd ("Go to checkout")
 @@ script infofile_;_ZIP::ssf26.xml_;_
Call FW_TransactionStart ("Free Shipping on order over $50")
Call FW_WebRadioGroup ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","methodId","5")
Call FW_TransactionEnd ("Free Shipping on order over $50") @@ script infofile_;_ZIP::ssf27.xml_;_

Call FW_TransactionStart ("Input OrderNote")
Call FW_WebEdit ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","notes",OrderNote)
Call FW_TransactionEnd ("Input OrderNote")
 
Call FW_TransactionStart ("Click Proceed to payment") @@ script infofile_;_ZIP::ssf28.xml_;_
Call FW_WebButton ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Proceed to payment") @@ script infofile_;_ZIP::ssf29.xml_;_
Call FW_TransactionEnd ("Click Proceed to payment")

Call FW_TransactionStart ("COD Cash On Delivery")
Call FW_WebRadioGroup ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","methodId","5")
Call FW_TransactionEnd ("COD Cash On Delivery")

Call FW_TransactionStart ("Place order")
Call FW_WebButton ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Place order") @@ script infofile_;_ZIP::ssf30.xml_;_
Call FW_TransactionEnd ("Place order") @@ script infofile_;_ZIP::ssf31.xml_;_

Call FW_TransactionStart ("Chack Message Thank you for your order")
Call FW_CheckWebElement ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Thank you for your order")
Call FW_TransactionEnd ("Chack Message Thank you for your order")
 @@ script infofile_;_ZIP::ssf32.xml_;_
 
Call FW_TransactionStart ("My account")
Call FW_WebElement ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","My account") @@ script infofile_;_ZIP::ssf33.xml_;_
Call FW_TransactionEnd ("My account")

Call FW_TransactionStart ("Log out")
Call FW_Link ("TC04_Xcart [TC04_Xcart]","X-Cart Demo store company","X-Cart Demo store company","Log out") @@ script infofile_;_ZIP::ssf34.xml_;_
Call FW_TransactionEnd ("Log out")
 @@ script infofile_;_ZIP::ssf56.xml_;_
'Call FW_TransactionStart ("CloseWebBrowser")
'Call FW_CloseCurrentBrowser()
'Call FW_TransactionEnd ("CloseWebBrowser")


